require('plugins')
require('bindings')
require('autocmd')
require('aliases')

vim.cmd('language en_US.UTF-8')

vim.opt.background = dark
vim.opt.backspace = 'indent,eol,start'
vim.opt.belloff = 'all'
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.laststatus = 2
vim.opt.number = true
vim.opt.ruler = true
vim.opt.scrolloff = 4
vim.opt.shiftwidth = 2
vim.opt.showmode = false
vim.opt.signcolumn = 'yes'
vim.opt.smartindent = true
vim.opt.softtabstop = 2
vim.opt.termguicolors = true
vim.opt.title = true
vim.opt.titlestring = '%r%m %y %t - Neovim'
vim.opt.updatetime = 100
vim.opt.wrap = false

vim.wo.fillchars='eob: '
