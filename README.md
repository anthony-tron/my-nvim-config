# Features

- Fast autocomplete
- File explorer (might be removed in the future, no keybinds yet but can be used with :Neotree)
- Fuzzy finder
- Javascript and Typescript linter (with support for ESLint)
- Monokai theme


# Install

Clone this repository in your nvim config directory. `git clone https://gitlab.com/anthony-tron/my-nvim-config.git ~/.config/nvim`

You must install these dependencies manually according to [this reference](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md):
- eslint
- quick-lint-js
- tsserver

Then:
- run `nvim +PackerSync`
- restart nvim and run `:COQdeps`
- restart nvim


# Customize

This configuration has the following structure:

```
├── init.lua         (various options go here)
└── lua
    ├── autocmd.lua  (all autocommands go here)
    ├── bindings.lua (all keymaps go here)
    └── plugins.lua  (all packer plugins go here)
```

