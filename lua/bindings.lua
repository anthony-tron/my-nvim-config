--
-- normal mode

-- map leader
vim.g.mapleader = ','

vim.keymap.set('n', '<Leader>d', '<Cmd>SignifyDiff<CR>')
vim.keymap.set('n', '<Leader>t', '<Cmd>tabclose<CR>')

-- neotree
vim.keymap.set('n', 'fp', '<Cmd>Neotree<CR>')

-- telescope
vim.keymap.set('n', 'ff', '<cmd>Telescope find_files<cr>')
vim.keymap.set('n', 'fg', '<cmd>Telescope live_grep<cr>')
vim.keymap.set('n', 'fb', '<cmd>Telescope buffers<cr>')

-- show documentation of symbol under the cursor
vim.keymap.set('n', '<F1>', '<Cmd>lua vim.lsp.buf.hover()<CR>')

-- follow symbol / jump to definition
vim.keymap.set('n', 'L', '<Cmd>lua vim.lsp.buf.definition()<CR>')

-- go to error
vim.keymap.set('n', '<F2>', '<Cmd>lua vim.diagnostic.goto_next()<CR>')
vim.keymap.set('n', '<S-F2>', '<Cmd>lua vim.diagnostic.goto_prev()<CR>')

-- trouble
vim.keymap.set('n', '<F3>', '<Cmd>TroubleToggle<CR>')

-- quickfix
vim.keymap.set('n', '<A-CR>', '<Cmd>lua vim.lsp.buf.code_action({\'quickfix\'})<CR>')

-- debugging
vim.keymap.set('n', '<Leader>b', '<Cmd>lua require("dap").toggle_breakpoint()<CR>')
vim.keymap.set('n', '<F7>', '<Cmd>lua require("dap").step_into()<CR>')
vim.keymap.set('n', '<F8>', '<Cmd>lua require("dap").step_over()<CR>')
vim.keymap.set('n', '<F9>', '<Cmd>lua require("dap").continue()<CR>')
vim.keymap.set('n', '<F10>', '<Cmd>lua require("dapui").toggle()<CR>')
vim.keymap.set('n', '<Leader><F1>', '<Cmd>lua require("dapui").eval()<CR>')
vim.keymap.set('n', '<Leader><F2>', '<Cmd>lua require("dapui").float_element("scopes", { height=8, width=48 })<CR>')

-- save
vim.keymap.set('n', '<leader>w', '<Cmd>w<CR>')

-- windows
vim.keymap.set('n', '<leader>q', '<C-W>q')
vim.keymap.set('n', '<leader>L', '<C-W>L')
vim.keymap.set('n', '<leader>K', '<C-W>K')
vim.keymap.set('n', '<leader>H', '<C-W>H')
vim.keymap.set('n', '<leader>J', '<C-W>J')
vim.keymap.set('n', '<leader>l', '<C-W>l')
vim.keymap.set('n', '<leader>k', '<C-W>k')
vim.keymap.set('n', '<leader>h', '<C-W>h')
vim.keymap.set('n', '<leader>j', '<C-W>j')

--
-- insert mode
vim.keymap.set('i', 'jk', '<Esc>')

