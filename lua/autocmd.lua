-- eslint --fix on save
vim.api.nvim_create_autocmd('BufWritePre', {
  command = 'EslintFixAll',
  pattern = '*.{js,jsx,ts,tsx}',
})

-- start COQ
vim.api.nvim_create_autocmd('VimEnter', {
  command = 'COQnow -s',
})

vim.api.nvim_create_autocmd('BufEnter', {
  command = 'set syntax=sql',
  pattern = '*.hdb*',
})

