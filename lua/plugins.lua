return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  use {
    'mfussenegger/nvim-dap',
    config = function()
      local dap = require('dap')

      dap.adapters.node2 = {
        type = 'executable',
        command = 'node',
        args = {os.getenv('HOME') .. '/dev/microsoft/vscode-node-debug2/out/src/nodeDebug.js'},
      }

      local nodeConfig = {
        {
          name = 'Launch',
          type = 'node2',
          request = 'launch',
          program = '${file}',
          cwd = vim.fn.getcwd(),
          sourceMaps = true,
          protocol = 'inspector',
          console = 'integratedTerminal',
        },
        {
          -- For this to work you need to make sure the node process is started with the `--inspect` flag.
          name = 'Attach to process',
          type = 'node2',
          request = 'attach',
          processId = require'dap.utils'.pick_process,
        },
      }

      dap.configurations.javascript = nodeConfig;
      dap.configurations.typescript = nodeConfig;
    end
  }

  use {
    'rcarriga/nvim-dap-ui',
    requires = {'mfussenegger/nvim-dap'},
    config = function()
      require('dapui').setup()
    end
  }

  use 'neovim/nvim-lspconfig'

  use 'mhinz/vim-signify'

  use 'tpope/vim-surround'

  use {
    'windwp/nvim-autopairs',
    config = function()
      require("nvim-autopairs").setup {}
    end
  }

  use {
    'ms-jpq/coq_nvim',
    branch = 'coq',
    config = function ()
      local lsp = require 'lspconfig'
      local coq = require 'coq'

      lsp.eslint.setup(coq.lsp_ensure_capabilities{})

      lsp.quick_lint_js.setup(coq.lsp_ensure_capabilities({
        filetypes = {
          'javascript',
	  'javascriptreact',
	  'typescript',
	  'typescriptreact',
        },
      }))

      lsp.tsserver.setup(coq.lsp_ensure_capabilities{})
    end,
  }

  use {
    'ms-jpq/coq.artifacts',
    branch = 'artifacts',
  }

  use {
    'ms-jpq/coq.thirdparty',
    branch = '3p',
  }

  use {
    'sainnhe/sonokai',
    config = function()
      vim.cmd('colo sonokai')
    end,
  }

  use {
    'lewis6991/gitsigns.nvim',
    tag = 'release',
    config = function()
      require('gitsigns').setup()
    end,
  }

  use {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v2.x',
    requires = {
      'nvim-lua/plenary.nvim',
      'MunifTanjim/nui.nvim',
    },
  }

  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
    },
  }

  use {
    'folke/trouble.nvim',
    config = function()
      require('trouble').setup({
        height = 7,
        icons = false,
        padding = false,
      })
    end
  }

  use {
    'nvim-lualine/lualine.nvim',
    config = function()
      require('lualine').setup {
        options = {
          icons_enabled = false,
          section_separators = { left = '', right = ''},
        },
        sections = {
          lualine_a = {
            {
              'mode',
              fmt = function(str)
                return str:sub(1,2)
              end
            },
          },
          lualine_b = {
            'filename'
          },
          lualine_c = {},
          lualine_x = {},
          lualine_y = {},
          lualine_z = {
            'branch'
          },
        },
        tabline = {
        },
        extensions = {
          'neo-tree',
        },
      }
    end
  }
end)

